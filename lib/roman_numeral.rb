class RomanNumeral
  ROMAN_CONVERSION = [
    [9000, "MXbar"], [5000, "Vbar"], [4000, "MVbar"], [1000, "M"],
    [900, "CM"],     [500, "D"],     [400, "CD"],     [100, "C"],
    [90, "XC"],      [50, "L"],      [40, "XL"],      [10, "X"],
    [9, "IX"],       [5, "V"],       [4, "IV"],       [1, "I"]
  ]

  def convert initial_value, result=""
    ROMAN_CONVERSION.each do |decimal_value, roman_value|
      return convert(initial_value - decimal_value, result + roman_value) if initial_value >= decimal_value
    end
    result
  end
end
