require 'roman_numeral'

describe RomanNumeral do
  let(:value) { 1 }
  subject { RomanNumeral.new.convert(value) }
  context 'when 1' do
    let(:value) { 1 }
    it { should == "I" }
  end

  context 'when 2' do
    let(:value) { 2 }
    it { should == "II" }
  end

  context 'when 3' do
    let(:value) { 3 }
    it { should == "III" }
  end

  context 'when 4' do
    let(:value) { 4 }
    it { should == "IV" }
  end

  context 'when 5' do
    let(:value) { 5 }
    it { should == "V" }
  end

  context 'when 6' do
    let(:value) { 6 }
    it { should == "VI" }
  end

  context 'when 9' do
    let(:value) { 9 }
    it { should == "IX" }
  end

  context 'when 10' do
    let(:value) { 10 }
    it { should == "X" }
  end

  context 'when 25' do
    let(:value) { 25 }
    it { should == "XXV" }
  end

  context 'when 254' do
    let(:value) { 254 }
    it { should == "CCLIV" }
  end

  context 'when 2541' do
    let(:value) { 2541 }
    it { should == "MMDXLI" }
  end
end
